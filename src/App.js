
import { AES, enc } from 'crypto-js';
import { useCallback, useState } from 'react';
import './assets/styles/styles.css';


function App() {
   const [encryptionData, setEncryptionData] = useState({ text: "", encrypted: "" });
   const [decryptionData, setDecryptionData] = useState({ cipher: "", decrypted: "" });
   const [buttonText, setButtonText] = useState('Copy');

   const handleChangeEncryption = useCallback((event) => {
      setEncryptionData({ ...encryptionData, text: event.target.value });
   }, []);

   const handleChangeDecryption = useCallback((event) => {
      setDecryptionData({ ...decryptionData, cipher: event.target.value });
   }, []);

   const handleEncrypt = useCallback(() => {
      console.log(encryptionData);
      const encrypted = AES.encrypt(encryptionData.text, 'secret_key');
      setEncryptionData({ ...encryptionData, encrypted: encrypted.toString() });
   }, [encryptionData]);

   const handleDecrypt = useCallback(() => {
      const bytes = AES.decrypt(decryptionData.cipher, 'secret_key');
      const decrypted = bytes.toString(enc.Utf8);
      setDecryptionData({ ...decryptionData, decrypted: decrypted });
      console.log(decryptionData);
   }, [decryptionData]);

   const handleCopyEncryption = useCallback(() => {
      const text = document.getElementById('ecrypted_text');
      navigator.clipboard.writeText(text.textContent);
      setButtonText('Copied !');
      setTimeout(() => {
         setButtonText('Copy');
      }, 1500);

   }, []);

   return (
      <div className="App">
         <div className='encryption-div'>
            <p>Enter text to encrypt it</p>
            <input type="text" name="enc" placeholder='Text goes here' onChange={handleChangeEncryption} />
            <button onClick={handleEncrypt}>Encrypt</button>
            {encryptionData.encrypted &&
               <pre className='encPre'>
                  <span id='ecrypted_text'>{encryptionData.encrypted}</span>
                  <button onClick={handleCopyEncryption}>{buttonText}</button>
               </pre>
            }
         </div>
         <div className='decryption-div'>
            <p>Enter cipher to decrypt it</p>
            <input type='text' name="decr" placeholder='Cipher goes here' onChange={handleChangeDecryption} />
            <button onClick={handleDecrypt}>Decrypt</button>
            {decryptionData.decrypted &&
               <pre className='descPre'>
                  <span id='ecrypted_text'>{decryptionData.decrypted}</span>
                  <button onClick={handleCopyEncryption}>{buttonText}</button>
               </pre>
            }
         </div>
      </div>
   );
}

export default App;
